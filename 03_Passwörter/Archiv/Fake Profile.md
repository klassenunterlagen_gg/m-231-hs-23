# Fake Profile 

# Firma

## Addresse
**Strauss Fricke AG**<br>
Schulstrasse 38<br>
8400 Winterthur


Seit 1955 steht der Name Strauß Fricke AG für Qualität, Zuverlässigkeit und Beständigkeit im Metallbau. In den Nachkriegsjahren gegründet, wuchs das Unternehmen stetig. Wir bieten Dienstleistungen in den Bereichen Schlosserei, Blechbearbeitung und Kunstschmiede an.

Als moderner KMU sind wir uns unserer Wurzeln bewusst und halten unsere Grundsätze hoch. Wir sind stolz, Lehrlinge auszubilden und in der Stadt und Umgebung unsere „Spuren“ zu hinterlassen.

Unsere Werte:
-   Innovative neue Ideen entwickeln und umzusetzen.
-   Reparaturen schnell und flexibel auszuführen.
-   Behutsam bestehendes zu restaurieren.

Besuchen Sie die Galerie und machen Sie sich selbst ein Bild über unsere vielfältigen Arbeiten.

# CEO

|                |                                                     |
|----------------|-----------------------------------------------------|
| Name           | Marcel Möller                                       |
| Adresse        | Landstrasse 12                                      |
| PLZ, Ort       | 9056 Schachen                                       |
| Telefon        | 071 465 64 35                                       |
| Geburtstag     | 30.10.1974                                          |
| E-Mail-Adresse | [Marcel.m@example.com](mailto:Marcel.m@example.com) |
| Auto           | 2009 Citroen C4                                     |
| Autonummer     | AR 371 372                                          |

# Administration

|                |                                         |
|----------------|-----------------------------------------|
| Name           | Tim Vogt                                |
| Adresse        | Rasenstrasse 60                         |
| PLZ, Ort       | 8330 Hermatswil                         |
| Telefon        | 044 597 72 96                           |
| Geburtstag     | 15.07.1985                              |
| E-Mail-Adresse | [tv@example.com](mailto:tv@example.com) |
| Auto           | 2006 Chevrolet Silverado                |
| Autonummer     | ZH 561 113                              |
