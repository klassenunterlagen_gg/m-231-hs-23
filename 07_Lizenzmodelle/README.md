# Lizenzmodelle

<sup>_(Diese Aufgabe ist für eine TEAMS-Aufgabe ausgelegt)_</sup>

Kompetenz und Lernziel siehe [Modulbaukasten](https://www.modulbaukasten.ch/module/231/1/de-DE?title=Datenschutz-und-Datensicherheit-anwenden) (Hz 6) - Sie kennen verschiedene Lizenzmodelle

Zeitbedarf ca. 60-90 min

Erstellen Sie eine eigene Seite in Ihrem Dossier für diese Aufgabe und **geben Sie am Schluss den Link auf diese Seite** ab.


- Aufgabe 1: Erklären Sie das Wort **Lizenz**. Was bedeutet das?

- Aufgabe 2: Warum kommt man auf die Idee, Lizenzen zu schaffen?

- Aufgabe 3: Worauf gibt es alles Lizenzen ausser für Software?

- Aufgabe 4: Machen Sie eine Aufzählung von Lizenzmodellen. 
  <br>Sie können dazu diese Quellen verwenden:
	- https://www.aagon.com/blog/software-lizenzmodelle
	- https://www.lizenzdirekt.com/expertenwissen/software-lizenzmodelle
	- https://cpl.thalesgroup.com/de/software-monetization/software-license-models
	- https://www.gnu.org/licenses/
	- https://de.wikipedia.org/wiki/GNU_General_Public_License
	- https://de.wikipedia.org/wiki/Lizenz

- Aufgabe 5: Was bedeutet "open source" und was kann man mit solcher Software machen?

- Aufgabe 6: Was ist der Unterschied zwischen "copyright" und "copyleft"

- Aufgabe 7: Frage: Welches Lizenz-Modell wird angewendet, wenn man 
	- im App-Store eine zu bezahlende App heunterlädt und installiert?
	- im App-Store eine Gratis-App heunterlädt und installiert?
	- Haben Sie die Software bezahlt, als Sie ihr aktuelles Smartphone gekauft/bekommen haben? Wenn Sie es nicht wissen, ob Sie extra bezahlt haben, wie und wann bezahlen Sie?
