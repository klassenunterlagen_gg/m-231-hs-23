# Internet als Datenarchiv

![Symbolbild](media/Suchbild.png)

| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einezelarbeit, Challenge |
| Aufgabenstellung  | Online Recherche |
| Zeitbudget  |  1 Lektion |
| Ziel | Informationen möglichst schnell/effizient im Internet finden. |


## Einleitung
Am 07.04.2022 erschien in 20min ein [Artikel](media/Artikel%2020Minuten.pdf) über einen Femizid in Hamburg. Im Artikel sind alle Namen abgekürzt und das Foto des Täters ist verpixelt.

## Ziel
Das Ziel des Auftrags ist es, festzustellen, wie schwierig (oder einfach?) es ist, aus eigentlich verschleierten Angaben Facts zu Datenschutz-relevanten Informationen zu erhalten.

## Auftrag
1.	Erstellen Sie eine Liste mit Informationen, welche Sie dem Text entnehmen können
2.	Versetzen Sie sich in die Lage eines Detektivs. Versuchen Sie mit diesen Informationen und mit Hilfe des Internets möglichst viele weitere Angaben und Fotos über den Täter (evtl. Opfer) zu erlangen.
3.	Stellen Sie die gesammelten Informationen vor 
⇒ Welche Gruppe findet die meisten Informationen?

