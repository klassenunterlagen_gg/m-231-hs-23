
# Checklisten des Datenschutzbeauftragten des Kantons Zürichs
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Prüfen der eigenen Infrastruktur mithilfe von Checklisten |
| Zeitbudget  |  1 bis 2 Lektionen |
| Ziel | Jeder Lernende hat einen Teil seiner persönlichen Infrastruktur geprüft, protokolliert und die notwendigen Verbesserungsmassnahmen festgelegt und wenn möglich umgesetzt.  |

## Aufgabenstellung
 - Besuchen Sie die Webseite https://www.datenschutz.ch/meine-daten-schuetzen
 - Wählen zwei Checklisten aus und gehen Sie diese Schrittweise durch. 
 - Notieren Sie zu jedem Punkt, ob Sie
   - das bereits erfüllt haben.
   - Wenn ja: Wie Sie haben diesen Punkt erfüllt?
   - Wenn nein: Was können Sie machen um diesen Punk zu erfüllen?
 - Wichtig: Sie müssen auch wissen, wie sie es umsetzen! (Siehe Beispiel unten)
 - Halten Sie Ihre Ergebnisse im Markdown-Format in einem Textdokument fest. Das wird später Teil Ihres Portfolios werden. 

## Markdownformat-Beispiel 
Ihre Dokumentation sollte ungefähr so aussehen:
```
# Checklisten des Datenschutzbeauftragten des Kantons Zürichs

# Checkliste Smartphone-Sicherheit erhöhen
 - Link: https://www.datenschutz.ch/meine-daten-schuetzen/smartphone-sicherheit-erhoehen
 - 01 Gerätesperren aktivieren
    - Ja, ich habe eine Gerätesperre mit PIN aktiviert. 
 - 02 Gerät bei Verlust sofort sperren und löschen lassen.
    - Kannte ich noch nicht, habe https://www.google.com/android/find besucht und mir die Funktionen angeschaut. Jetzt weiss ich wie ich im Ernstfall handeln muss. 
 - 03 Apps aus vertrauenswürdigen Quellen installieren
    - Ich installiere alle Apps aus dem Google Playstore und prüfe bei jeder App, welche Zugriffsrechte Sie hat. Ich bin alle Berechtigungseinstellungen für jedes App im meinem Telefon durchgegangen und wenn nötig Änderungen vorgenommen
 - 04 ...
```

## Hinweis
Diese Aufgabe ist Teil der [Leistungsbeurteilung LB3](../99_Leistungsbeurteilung/). 
